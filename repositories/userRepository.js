const {BaseRepository} = require('./baseRepository');

class UserRepository extends BaseRepository {
    constructor() {
        super('users');
    }

    create(data) {
        return super.create(data);
    }

    getOne(search) {
        return super.getOne(search);
    }

    update(id, dataToUpdate) {
        return super.update(id, dataToUpdate);
    }

    delete(id) {
        return super.delete(id);
    }

    getAll() {
        return super.getAll();
    }

}

exports.UserRepository = new UserRepository();