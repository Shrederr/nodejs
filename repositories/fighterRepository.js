const {BaseRepository} = require('./baseRepository');

class FighterRepository extends BaseRepository {
    constructor() {
        super('fighters');
    }

    create(data) {
        return super.create(data);
    }

    getOne(search) {
        return super.getOne(search);
    }

    update(id, dataToUpdate) {
        return super.update(id, dataToUpdate);
    }

    delete(id) {
        return super.delete(id);
    }
}

exports.FighterRepository = new FighterRepository();