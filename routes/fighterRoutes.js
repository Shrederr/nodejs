const {Router} = require('express');
const FighterService = require('../services/fighterService');
const {responseMiddleware} = require('../middlewares/response.middleware');
const {createFighterValid, updateFighterValid} = require('../middlewares/fighter.validation.middleware');
const router = Router();

router.post('/', createFighterValid, async (req, res, next) => {
    try {
        const {name, power, defense} = req.body;
        res.data = await FighterService.createFighter({name, power, defense});
        next();
    } catch (error) {
        res.status(400).send({
            error: true,
            message: error.message
        });
    }
}, responseMiddleware);

router.get('/', (req, res, next) => {
    try {
        res.data = FighterService.getAllFighters();
        next();
    } catch (error) {
        res.status(404).send({
            error: true,
            message: error.message
        });
    }
}, responseMiddleware);

router.get(`/:id`, (req, res, next) => {
    try {
        const id = req.params.id;
        res.data = FighterService.getOneFighter(id);
        next();
    } catch (error) {
        res.status(404).send({
            error: true,
            message: error.message
        });
    }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    try {
        const id = req.params.id
        res.data = FighterService.updateFighterInfo(id, req.body);
        next();
    } catch (error) {
        res.status(400).send({
            error: true,
            message: error.message
        });
    }
}, responseMiddleware);

router.delete(`/:id`, (req, res, next) => {
    try {
        const id = req.params.id
        res.data = FighterService.deleteFighter(id);
        next();
    } catch (error) {
        res.status(404).send({
            error: true,
            message: error.message
        });
    }
}, responseMiddleware);


module.exports = router;