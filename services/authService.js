const UserService = require('./userService');

class AuthService {
    login(userData) {
        const user = UserService.getOneUser({email: userData.email});
        if (!user) {
            throw new Error('User not found');
        }
        if (userData.password !== user.password) {
            throw new Error('Password is not correct');
        }
        return user;
    }
}

module.exports = new AuthService();