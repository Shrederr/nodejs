const {FighterRepository} = require('../repositories/fighterRepository');

class FighterService {

    async createFighter({name, power, defense}) {
        alreadyExist({name}, 'Fighter with such name exists');
        return FighterRepository.create({name, power, defense, health: 100});
    }

    getAllFighters() {
        return FighterRepository.getAll(FighterRepository.constructor);
    }

    getOneFighter(id) {
        return isExist(id, "Such fighter does not exist");
    }

    updateFighterInfo(id, dataToUpdate) {
        const candidateForUpdate = isExist(id, "There is no such fighter");
        const arrayOfCandidatesKeys = Object.keys(dataToUpdate);
        arrayOfCandidatesKeys.forEach((field) => {
            if (dataToUpdate[field] === candidateForUpdate[field]) {
                delete dataToUpdate[field];
            }
        });
        alreadyExist({name: dataToUpdate.name}, 'Fighter with such name exists')
        return FighterRepository.update(id, dataToUpdate);
    }

    deleteFighter(id) {
        const candidateForDelete = isExist(id, "There is no such fighter");
        FighterRepository.delete(id);
        return candidateForDelete;
    }
}

const isExist = function (id, message) {
    const isExist = FighterRepository.getOne({id});
    if (!isExist) {
        throw new Error(
            message
        );
    }
    return isExist;
}

const alreadyExist = function (parameter, message) {
    const alreadyExist = FighterRepository.getOne(parameter);
    if (alreadyExist) {
        throw new Error(
            message
        );
    }
}

module.exports = new FighterService();