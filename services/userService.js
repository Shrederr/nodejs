const {UserRepository} = require('../repositories/userRepository');

class UserService {

    async createUser({email, password, firstName, lastName, phoneNumber}) {
        alreadyExist({email}, 'A user with this mail already exists');
        alreadyExist({phoneNumber}, 'A user with this phone number already exists');
        return UserRepository.create({email, password, firstName, lastName, phoneNumber});
    }

    getAllUsers() {
        return UserRepository.getAll(UserRepository.constructor);
    }

    getOneUser(id) {
        return isExist(id, "Such user does not exist");
    }

    updateUserInfo(id, dataToUpdate) {
        const candidateForUpdate = isExist(id, "Such user does not exist");
        const arrayOfCandidatesKeys = Object.keys(dataToUpdate);
        arrayOfCandidatesKeys.forEach((field) => {
            if (dataToUpdate[field] === candidateForUpdate[field]) {
                delete dataToUpdate[field];
            }
        });
        alreadyExist({email: dataToUpdate.email}, 'A user with this mail already exists');
        alreadyExist({phoneNumber: dataToUpdate.phoneNumber}, 'A user with this phone number already exists');
        return UserRepository.update(id, dataToUpdate);
    }

    deleteUser(id) {
        const candidateForDelete = isExist(id, 'This user does not exist')
        UserRepository.delete(id);
        return candidateForDelete;
    }
}

const isExist = function (id, message) {
    const isExist = UserRepository.getOne({id});
    if (!isExist) {
        throw new Error(
            message
        );
    }
    return isExist;
}

const alreadyExist = function (parameter, message) {
    const alreadyExist = UserRepository.getOne(parameter);
    if (alreadyExist) {
        throw new Error(
            message
        );
    }
}
module.exports = new UserService();