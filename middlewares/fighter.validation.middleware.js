const {fighter} = require('../models/fighter');
const createFighterValid = (req, res, next) => {
    delete fighter.id;
    delete fighter.health;
    const arrayOfValidation = Object.keys(fighter);
    const candidateForCreate = req.body;
    const arrayOfRequestKeys = Object.keys(candidateForCreate);
    if (candidateForCreate.id) {
        res.status(400).send({
            error: true,
            message: `id forbidden field in the request body`
        });
    }
    requestValid(arrayOfValidation, arrayOfRequestKeys, 'You need to fill in the field', res);
    checkNameForValidity(candidateForCreate.name, 'incorrect filling of the field names of fighter', res);
    checkPowerForValid(candidateForCreate.power, 'incorrect filling of the field powers fighter', res);
    checkDefenseNumberForValid(candidateForCreate.defense, 'incorrect filling of the field  defenses fighter', res);

    next();
}

const updateFighterValid = (req, res, next) => {
    delete fighter.id;
    delete fighter.health;
    const arrayOfValidation = Object.keys(fighter);
    const candidateForUpdate = req.body;
    const arrayOfRequestKeys = Object.keys(candidateForUpdate);
    requestValid(arrayOfRequestKeys, arrayOfValidation, 'Invalid field in request body', res);
    if (candidateForUpdate.name) {
        checkNameForValidity(candidateForUpdate.name, 'incorrect filling of the field names of fighter', res);
    }
    if (candidateForUpdate.power) {
        checkPowerForValid(candidateForUpdate.power, 'incorrect filling of the field powers fighter', res);
    }
    if (candidateForUpdate.defense) {
        checkDefenseNumberForValid(candidateForUpdate.defense, 'incorrect filling of the field  defenses fighter', res);
    }
    next();
}

const requestValid = function (arrayOfValid, arrayToValidation, message, res) {
    arrayOfValid.forEach((field) => {
        if (!arrayToValidation.includes(field)) {
            res.status(400).send({
                error: true,
                message: message + " " + field
            });
        }
    });
}

const checkNameForValidity = function (name, message, res) {
    if (!/^[A-Za-zА-Яа-я]{3}/.test(name)) {
        res.status(400).send({
            error: true,
            message: message
        });
    }
}

const checkPowerForValid = function (power, message, res) {
    if (power > 100 || !Number.isInteger(power) || power < 0) {
        res.status(400).send({
            error: true,
            message: message
        });
    }
}

const checkDefenseNumberForValid = function (defense, message, res) {
    if (defense > 10 || !Number.isInteger(defense) || defense < 0) {
        res.status(400).send({
            error: true,
            message: message
        });
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;