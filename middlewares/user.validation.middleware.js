const {user} = require('../models/user');

const createUserValid = (req, res, next) => {
    delete user.id;
    const arrayOfValidation = Object.keys(user);
    const candidateForCreate = req.body;
    const arrayOfRequestKeys = Object.keys(candidateForCreate);
    if (candidateForCreate.id) {
        res.status(400).send({
            error: true,
            message: `id forbidden field in the request body`
        });
    }
    requestValid(arrayOfValidation, arrayOfRequestKeys, 'You need to fill in the field', res);
    checkMailForValid(candidateForCreate.email, 'The entered email is not valid', res);
    checkPasswordForValid(candidateForCreate.password, 'Password is no correct', res);
    checkPhoneNumberForValid(candidateForCreate.phoneNumber, 'PhoneNumber is no correct', res)

    next();
}

const updateUserValid = (req, res, next) => {
    delete user.id;
    const arrayOfValidation = Object.keys(user);
    const candidateForUpdate = req.body;
    const arrayOfRequestKeys = Object.keys(candidateForUpdate);
    requestValid(arrayOfRequestKeys, arrayOfValidation, 'Invalid field in request body', res);

    if (candidateForUpdate.email) {
        checkMailForValid(candidateForUpdate.email, 'The entered new email is not valid', res);
    }

    if (candidateForUpdate.password) {
        checkPasswordForValid(candidateForUpdate.password, 'New password is no correct', res);
    }

    if (candidateForUpdate.phoneNumber) {
        checkPhoneNumberForValid(candidateForUpdate.phoneNumber, 'New phoneNumber is no correct', res)
    }

    next();
}

const requestValid = function (arrayOfValid, arrayToValidation, message, res) {
    arrayOfValid.forEach((field) => {
        if (!arrayToValidation.includes(field)) {
            res.status(400).send({
                error: true,
                message: message + " " + field
            });
        }
    });
}

const checkMailForValid = function (email, message, res) {
    if (!/^[A-Za-z][A-Za-z0-9.]{4,}@gmail\.com$/.test(email)) {
        res.status(400).send({
            error: true,
            message: message
        });
    }
}

const checkPasswordForValid = function (password, message, res) {
    if (!/^[A-Za-z0-9][A-Za-z0-9.]{3}/.test(password)) {
        res.status(400).send({
            error: true,
            message: message
        });
    }
}

const checkPhoneNumberForValid = function (phoneNumber, message, res) {
    if (!/^[+]380[0-9]/.test(phoneNumber) || (phoneNumber).length !== 13) {
        res.status(400).send({
            error: true,
            message: message
        });
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;